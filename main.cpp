#include <iostream>
#include <bitset>
#include <chrono>
#include <thread>
#include <mutex>
#include <vector>


using namespace std;

#define firstBool 0b1
#define secondBool 0b10
#define thirdBool 0b100

mutex m;

void printBit(int bits)
{
    cout << bitset<8>(bits);
}

void checkBit(int bits, int n)
{
    cout << "bits: " << bitset<8>(bits) << endl;
    if(bits & (1 << n-1))
    {
        cout << "bit nr: " << n << " is on." << endl;
    }
    else
        cout << "bit nr: " << n << " is off." << endl;
}

int clearAllBits(int bits)
{
    for(int i = 32; i > 0; i--)
    {
        bits = bits & ~(1 << i);
    }
    return bits;
}

int toggleBits(int bits, unsigned int n)
{
    return bits ^ (1 << n-1);;   
}

void loadingThread(string threadName, long long amount, long long *percent)
{
    //cout << "START: " << threadName << endl;
    while(*percent < amount)
    {
        //cout << threadName << endl;
        //cout << "percent: " << bitset<32>(*percent) << "     amount: " << bitset<32>(amount) << endl;
        lock_guard<mutex> lock{m};
        *percent += 1;
        
        //std::this_thread::sleep_for (std::chrono::milliseconds(5000));
    }
    //cout << "Complete: " << threadName << endl;
}

void printThread(long long *first, long long amount)
{
    while(*first < amount)
    {
        //cout << "First: " << *first << endl;
        //this_thread::sleep_for (chrono::milliseconds(5000));
    }
}

int main()
{
    typedef chrono::system_clock Clock;

    unsigned int a = 0b0 | 0b10 | 0b000 | 0b101110000000;
    // 001 + 010 + 100 = 111 because "or"
    unsigned int b = 0b10101 & 0b0100;
    // 10101 + 00100 = 00100 because "and"

    a = a ^ firstBool;
    // xor 0 + 0 = 0 | 1 + 0 = 1 | 1 + 1 = 0 So in this example it will always change to the oposite of what it allready is

    a = a & ~secondBool;
    // turn bools off

    a = a | thirdBool;
    // turn bools on

    //for(int i = 1; i <= 12; i++)
        //checkBit(a, i);

    long long amount = 999999999;

    long long first = 0, second = 0, third = 0;
    auto t1 = Clock::now();

    vector<thread> Threads;

    for(int i = 0; i < 2; i++)
    {
        Threads.push_back(thread(loadingThread, to_string(i), amount, &first));
        cout << "Starting thread " << i << endl;
    }

        cout << "All " << Threads.size() << " threads started!" << endl;

    for(int i = 0; i < Threads.size(); i++)
    {
        Threads[i].join();
        cout << "joining: " << i << endl;
    }







    auto t2 = Clock::now();
    cout << Threads.size() << " threads time: " << chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count() << " ms" << '\n';


    t1 = Clock::now();
    first = 0, second = 0, third = 0;
    loadingThread("first", amount, &first);



    t2 = Clock::now();
    cout << "Single thread time: " << chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count() << " ms" << '\n';

    a = clearAllBits(a);
    b = clearAllBits(b);

    for (int i = 0; i < 8; i++)
    {
        a = toggleBits(a, i);
        b = toggleBits(b, 9-i);
        printBit(a);

        //cout << "tall: " << a << endl;
        printBit(b);
        cout << endl;
        std::this_thread::sleep_for (std::chrono::milliseconds(10));
    }

    for (int i = 7; i > 0; i--)
    {
        a = toggleBits(a, i);
        b = toggleBits(b, 9-i);
        printBit(a);
        printBit(b);
        std::this_thread::sleep_for (std::chrono::milliseconds(10));
        cout << endl;
    }

    cout << "COMPLETE" << endl;

    //checkBit(a);
    //checkBit(b);
    // cout << a << endl;
}